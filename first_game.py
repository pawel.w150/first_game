import random

# The draw function is responsible for drawing the rock, paper, scissors element
# by the computer
def draw():
    if random.randint(0,2) == 0:
        return "rock"
    elif random.randint(0,2) == 1:
        return "paper"
    elif random.randint(0,2) == 2:
        return "scissors"

# The game function compares the selection of the computer and the user
def game():
    y = input("Pass the paper, rock or scissors: ")
    
   
    if y == "rock" and draw() == "paper":
        print("The computer drew paper, You lost")
    elif y == "rock" and draw() == "scissors":
        print("The computer drew scissors, You win")
    elif y == "paper" and draw() == "scissors":
        print("The computer drew scissors, You lost")
    elif y == "paper" and draw() == "rock":
        print("The computer drew rock, You win")
    elif y == "scissors" and draw() == "rock":
        print("The computer drew rock, You lost")
    elif y == "scissors" and draw() == "paper":
        print("The computer drew paper, You win")
    else:
        print(f"The computer selected {y}. There is a draw in the game")
            
# Program control loop
while True:
    x = input("This is a rock, paper, scissors game. "
              "If you want to play press: Y, if you want to unsubscribe, press: N: ")

    if x == "Y":
        print("Well done, you're getting into the game")
        game()
        z = input("If you want to play again press Y: "
                  "If you want to unsubscribe, press N: ")
        if z == "Y":
            game()
        elif z == "N":
            print("You leave the game.")
            break
    elif x == "N":
        print("You have been logged out")
        break
    else:
        print("You have entered an incorrect value,"
              "please enter: Y or N: ")

    

    
            

              
